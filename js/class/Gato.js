export default class Gato extends HTMLElement {
    constructor () {
        super()
        this.currentMovements = undefined
        this.currentPlayer = undefined
    }
    connectedCallback() {
        this.initGato()
    }

    disconnectedCallback() {
        console.log('Removiendo Gato WebComp')
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if(oldValue){
            this.removeChild(this.childNodes[0])
            this.reset()
            this.initGato()
        }
    }

    static get observedAttributes() {
        return ['firstplayer', 'size'];
    }

    initGato(){
        let i, button, div = document.createElement('div')

        if(isNaN(this.rawSize)) {
            this.size = '3'
        }

        if(this.firstPlayer !== 'O' && this.firstPlayer !== 'X') {
            this.firstPlayer = 'O'
        }
        
        this.currentMovements = new Array(this.size * this.size)
        this.currentMovements.fill('-')
        

        this.currentPlayer = this.firstPlayer
 
        for (i = 0; i < this.size * this.size ; i++) {
            button = document.createElement("button")
            button.setAttribute('type', 'button')
            button.setAttribute('value', i)
            button.innerHTML = '-'
            button.onclick = event => this.handleMovement(event)
            div.appendChild(button)
        }
        this.appendChild(div)
    }

    reset(){
        this.currentMovements = undefined
        this.currentPlayer = undefined
    }

    handleMovement (event) {
        if(event.srcElement.outerText === '-'){
            this.currentMovements[parseInt(event.srcElement.value)] = this.currentPlayer
            if(this.checkWinner(this.getRowChunk()) || this.checkWinner(this.getColumnChunk()) || (this.size % 2 && this.checkWinner(this.getDiagonalChunk()))){
                alert('Ganador ' + this.currentPlayer)
                this.removeChild(this.childNodes[0])
                this.reset()
                this.initGato()
            }
            event.srcElement.innerHTML = this.currentPlayer
            this.currentPlayer = this.currentPlayer === 'O' ? 'X' : 'O'
        }
    }
    

    checkWinner (chunk) {
        for (let index = 0; index < chunk.length; index++) {
            const element = chunk[index]
            if(element.every(el => el === 'O') || element.every(el => el === 'X')) return true
        }
        return false
    }

    getRowChunk() {
        var i, temparray, chunk
        temparray = []
        chunk = this.size
        for (i = 0 ; i < this.currentMovements.length; i += chunk){
            temparray.push(this.currentMovements.slice(i, i + chunk))
        }
        return temparray
    }

    getColumnChunk() {
        var i, j, temparray
        temparray = []
        for (i = 0 ; i < this.size; i++) {
            temparray.push([])
            for (j = i; j <= (this.size * (this.size -1)) + i; j += this.size){
                temparray[i].push(this.currentMovements[j])
            }   
        }
        return temparray
    }

    getDiagonalChunk(){
        var i, j, k, temparray
        temparray = [[],[]]
        for (i = 0, j = this.size * (this.size -1), k = 0; i < this.size; i++, j -= (this.size - 1), k += (this.size + 1)) {
            temparray[0].push(this.currentMovements[k])
            temparray[1].push(this.currentMovements[j])
        }
        return temparray
    }

    get size (){
        return parseInt(this.getAttribute('size'))
    }
    get rawSize (){
        return this.getAttribute('size')
    }
    
    set size (newValue) {
        this.setAttribute('size', newValue)
    }

    get firstPlayer (){
        return this.getAttribute('firstPlayer')
    }
    
    set firstPlayer (newValue) {
        this.setAttribute('firstPlayer', newValue)
    }

} 