import Gato from "./class/Gato.js";

var size = parseInt(prompt('Ingresa el tamaño del gato')) || 3
var firstPlayer = confirm("¿Primero 'O'?") ? 'O' : 'X'

customElements.define('gato-web-component', Gato)

var gatoWebComponent = document.createElement('gato-web-component')
gatoWebComponent.setAttribute('size', size)
gatoWebComponent.setAttribute('firstPlayer', firstPlayer)

document.body.appendChild(gatoWebComponent)
